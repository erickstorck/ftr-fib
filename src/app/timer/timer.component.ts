import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  @Input() time: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
