import { Component, Input, OnInit } from '@angular/core';

export interface inputItem {
  number: number,
  frequency: number,
  time: number[]
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  time: number = 0
  maxTime: number = 0
  userFirstInput: number = NaN
  userSecondInput: number = NaN
  interval: any
  timeout: any
  haltTimer: number = 0

  inputList: inputItem[] = []

  enabler: any = {
    firstInput: false,
    secondInput: false,
    finalMessage: false
  }

  constructor() {
  }

  ngOnInit(): void {
    this.timerStart()
  }

  outputFreqChange() {
    this.maxTime = this.userFirstInput
  }

  timerStart() {
    this.interval = setInterval(() => {
      this.time += 1
      if (this.time > this.maxTime) {
        this.showList()
        this.time = 0
      }
    }, 1000)
  }

  showList(time = 1000) {
    this.enabler['finalMessage'] = true
    this.timeout = setTimeout(() => {
      this.enabler['finalMessage'] = false
    }, (this.maxTime * time) / 2);
  }

  finish() {
    this.enabler['firstInput'] = true
    this.enabler['secondInput'] = true
  }

  insertItem() {
    if (!isNaN(this.userSecondInput)) {
      this.isFibonacci(this.userSecondInput)
      let aux = this.inputList.find(f => f['number'] == this.userSecondInput)
      if (aux) {
        aux['time'].push(this.time)
        aux['frequency'] += 1
      } else {
        this.inputList.push({
          number: this.userSecondInput,
          time: [this.time],
          frequency: 1
        })
      }
      this.userSecondInput = NaN
      this.inputList.sort((a, b) => a['frequency'] < b['frequency'] ? 1 : -1)
    }
  }

  halt() {
    this.haltTimer = this.interval
    clearInterval(this.interval)
  }

  isFibonacci(n: number) {
    var a = (5 * Math.pow(n, 2) + 4)
    var b = (5 * Math.pow(n, 2) - 4)

    var result = Math.sqrt(a) % 1 == 0
    var res = Math.sqrt(b) % 1 == 0

    if (result || res == true) {
      this.enabler['fib'] = true
      this.timeout = setTimeout(() => {
        this.enabler['fib'] = false
      }, 1500);
    }
  }

  resume() {
    this.haltTimer == this.interval ? this.timerStart() : alert('timer active')
  }

  reset() {
    location.reload()
  }

  quit() {
    clearTimeout(this.timeout)
    this.finish()
    this.showList(100000)
    this.halt()
  }
}
